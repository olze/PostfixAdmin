{strip}
{include file="header.tpl"}
{include file="flash_error.tpl"}
<div id="register">
    <form name="frmRegister" method="post" action="">
        <table id="login_table" cellspacing="10">
            <tr>
                <th colspan="2">
                    {$PALANG.pUsersRegister_welcome}
                </th>
            </tr>
            <tr>
                <td class="label"><label>{$PALANG.pLogin_username}:</label></td>
                <td><input class="flat" type="text" name="fUsername" />@<select class="flat" name="$domain">
                        {html_options output=$domains  values=$domains}
                    </select></td>
                <td><input class="button" type="submit" name="check" value="{$PALANG.pCheckMail_button}" /></td>
            </tr>
            <tr>
                <td class="label"><label>{$PALANG.password}:</label></td>
                <td><input class="flat" type="password" name="fPassword" /></td>
            </tr>
            <tr>
                <td class="label"><label>{$PALANG.password_again}:</label></td>
                <td><input class="flat" type="password" name="fPasswordVerification" /></td>
            </tr>
            <tr>
                <td class="label"><label>{$PALANG.pLogin_language}:</label></td>
                <td>{$language_selector}</td>
            </tr>
            <tr>
                <td><img id="captcha" src="../securimage/securimage_show.php" alt="CAPTCHA Image" /></td>
                <td><input type="text" name="captcha_code" size="10" maxlength="6" />
                    <a href="#" onclick="document.getElementById('captcha').src = '../securimage/securimage_show.php?' + Math.random();
                            return false;">[ Different Image ]</a></td>
            </tr>
            <tr>
                <td class="label">&nbsp;</td>
                <td><input class="button" type="submit" name="register" value="{$PALANG.pRegister_button}" /></td>
            </tr>
            {if $logintype == 'admin'}
                <tr>
                    <td colspan="2"><a href="users/">{$PALANG.pLogin_login_users}</a></td>
                </tr>
            {/if}
        </table>
    </form>
    {literal}
        <script type="text/javascript">
            <!--
        document.frmRegister.fUsername.focus();
            // -->
        </script>
    {/literal}
</div>
{strip}
{include file="footer_empty.tpl"} 

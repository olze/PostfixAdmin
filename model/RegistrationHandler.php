<?php

/**
 * This class handles everything with registrations.
 *
 * @author oli
 */
class RegistrationHandler extends PFAHandler {

    protected function initMsg() {
        $this->msg['error_blocked'] = 'you have been blocked.';
    }

    function getUserIP() {
        $client = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote = $_SERVER['REMOTE_ADDR'];

        if (filter_var($client, FILTER_VALIDATE_IP)) {
            $ip = $client;
        } elseif (filter_var($forward, FILTER_VALIDATE_IP)) {
            $ip = $forward;
        } else {
            $ip = $remote;
        }

        return $ip;
    }

    protected function initStruct() {
        $this->struct = array(
            # field name                allow       display in...   type    $PALANG label                     $PALANG description                 default / options / ...
            #                           editing?    form    list
            'email' => pacol(0, 0, 0, 'mail', '', '', ''),
            'ip' => pacol(0, 0, 0, 'text', '', '', ''),
            'requestedAt' => pacol(0, 0, 0, 'ts', '', '', '', ''),
            'acceptedAt' => pacol(0, 0, 0, 'ts', '', '', ''),
            'blockedAt' => pacol(0, 0, 0, 'pass', 'ts', '', ''),
        );
    }

    protected function validate_new_id() {
        if ($this->id == '') {
            $this->errormsg[$this->id_field] = 'Wierdo error';
            return false;
        }

        return true;
    }

    public function webformConfig() {

        return array(
        );
    }

    public function isAllowedToRegisterAnEMail() {
        
    }

}

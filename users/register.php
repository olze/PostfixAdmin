<?php


require_once('../common.php');
require_once('../securimage/securimage.php');
$_SESSION['sessid']['username'] = "SELF_REGISTRATION";

$securimage = new Securimage();

if ($CONF['allow_self_register'] != true) {
    exit();
}

if ($CONF['configured'] !== true) {
    print "Installation not yet configured; please edit config.inc.php or write your settings to config.local.php";
    exit;
}

$mailboxHandler = new MailboxHandler(1);
$registrationHandler = new RegistrationHandler();

$domainhandler = new DomainHandler(0);
$list = $domainhandler->getList('');
$result = $domainhandler->result();
$domainList = array();
foreach ($result as $key => $value) {
    $domainList[] = $key;
}

if (isset($_POST["check"])) {
    $user = escape_string($_POST["fUsername"] . "@" . safepost('$domain'));
    if (!$mailboxHandler->init($user)) {
        $error = 1;
        $errormsg = $mailboxHandler->errormsg;
        flash_error($mailboxHandler->errormsg);
        header("Location: /postfixadmin/users/register.php");
    } else {
        flash_info("Available."); //TODO: use the messages from the mail handler
        header("Location: /postfixadmin/users/register.php");
    }
    exit();
}

if (isset($_POST["register"])) {
    if ($securimage->check($_POST['captcha_code']) == false) {
        flash_error("Wrong captcha code entered. Please try again.");
	header("Location /postfixadmin/users/register.php");
    }

    //do some validation to stop spam/bots/requests to register multiple mail adresses
    if(!$registrationHandler->isAllowedToRegisterAnEMail())
    {
        error_log("PostfixAdmin register failed (not allowed to register: $fUsername " . $registrationHandler->getUserIP());
        flash_error("Not allowed to register. If you think this is an error, write me an email.");
        header("Location: /users/register.php");
    	exit();
    }
    
    $fLocalPart = safepost('fUsername');
    $fDomain = safepost('$domain');
    $fUsername = $fLocalPart . "@" . $fDomain;

    $fPassword = safepost('fPassword');
    $fPassword2 = safepost('fPasswordVerification');

    if (in_array($fLocalPart, $CONF['forbidden_local_parts'])) {
        error_log("PostfixAdmin register failed (forbidden username: $fUsername)");
        //TODO: register that message in the handler
        flash_error("Not allowed to register that username. Please use another one.");
        header("Location: /users/register.php");
    	exit();
    }

    $validpass = validate_password($fPassword);
    if (count($validpass) > 0) {
        flash_error($validpass[0]); # TODO: honor all error messages, not only the first one
        $error = 1;
    }

    if (!$mailboxHandler->init($fUsername)) {
        $errormsg = $mailboxHandler->errormsg;
        flash_error($mailboxHandler->errormsg);
        header("Location: /postfixadmin/users/register.php");
	exit();
    }

    $values = [
        'username' => $fUsername,
        'password' => $fPassword,
        'password2' => $fPassword2,
        'quota' => '0',
        'active' => '0',
        'welcome_mail' => '1',
    ];

    if (!$mailboxHandler->set($values)) {
        $error = 1;
        $errormsg = $mailboxHandler->errormsg;
	flash_error($mailboxHandler->errormsg);
    }

    $form_fields = $mailboxHandler->getStruct(); # refresh $form_fields - set() might have changed something

    if ($error != 1) {
        if (!$mailboxHandler->store()) {
            $errormsg = $mailboxHandler->errormsg;
        } else {
            flash_info($mailboxHandler->infomsg);

            if (count($mailboxHandler->errormsg)) { # might happen if domain_postcreation fails
                flash_error($mailboxHandler->errormsg);
            }

            # remember prefill values for next usage of the form
            if (isset($formconf['prefill'])) {
                foreach ($formconf['prefill'] as $field) {
                    if (isset($values[$field])) {
                        $_SESSION["prefill:$table:$field"] = $values[$field];
                    }
                }
            }
            //TODO
            if ($edit != "") {
                header("Location: /users");
                exit;
            } else {
                if ($CONF['info_self_register'] == true) {
                    $fTo = smtp_get_admin_email();
                    $subject = 'New Registration recieved.';
                    $message = "A new user has been registered ($fUsername)";
                    $fHeaders = 'To: ' . $fTo . "\n";
                    $fHeaders .= 'From: ' . $fTo . ' <' . $fTo . ">\n";
                    $fHeaders .= 'Subject: ' . $subject . "\n";
                    $fHeaders .= 'MIME-Version: 1.0' . "\n";
                    $fHeaders .= 'Content-Type: text/plain; charset=UTF-8' . "\n";
                    $fHeaders .= 'Content-Transfer-Encoding: base64' . "\n";

		    $message = $fHeaders . $message;
			
                    if (!smtp_mail($fTo, $smtp_from_email, $subject, $message)) {
                        flash_error("<br />" . $PALANG['pSendmail_result_error'] . "<br />");
                    } else {
                        flash_info("<br />" . $PALANG['pSendmail_result_success'] . "<br />");
                    }
                }
                flash_info($mailboxHandler->infomsg);
                $url = $CONF['self_register_redirect_url'];
                header("Location: $url");
                exit;
            }
        }
    }
}

$smarty->assign('language_selector', language_selector(), false);
$smarty->assign('smarty_template', 'register');
$smarty->assign('logintype', 'user');
$smarty->assign('domains', $domainList);
$smarty->display('register.tpl');
